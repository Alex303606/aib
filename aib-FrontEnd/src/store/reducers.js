import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS} from "./actionTypes";

const initialState = {
	messages: [],
	error: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			if (action.data.error) {
				alert(action.data.error);
				return {...state, message: action.data.error, author: action.data.error, error: true};
			} else {
				return {...state, messages: action.data, error: false};
			}
		case FETCH_GET_SUCCESS:
			return {...state, messages: action.messages, error: false};
		default:
			return state;
	}
};

export default reducer;
